#include <cstdio>
#include <cstdlib>
#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include "Binder.h"

using namespace std;
typedef map<string, string> StrDict;

StrDict ParsePaths(const char** argv);
void CheckArgs(int argc);


int Bind(const char** argv) {
    auto params = ParsePaths(argv);
    string target_path = params["dst_folder"] + "/" + params["host_file"];
    Binder binder(params["self"], params["src_folder"]);
    binder.BindTo(target_path);

    return EXIT_SUCCESS;
}


int SelfDecompress(const char** argv) {
    string my_name(argv[0]);
    Binder::Decompress(my_name);

    return EXIT_SUCCESS;
}


int main(int argc, const char** argv)
{
    if (argc == 4) {
        return Bind(argv);
    } else if (argc == 1) {
        return SelfDecompress(argv);
    } else {
        fprintf(stderr, "Wrong arguments: need 3 or 1, given %d\n", argc);
        exit(EXIT_FAILURE);
    }
}


StrDict ParsePaths(const char** argv) {
    StrDict filenameDict;

    filenameDict["self"] = argv[0];
    filenameDict["src_folder"] = argv[1];
    filenameDict["dst_folder"] = argv[2];
    filenameDict["host_file"] = argv[3];

    return filenameDict;
}
