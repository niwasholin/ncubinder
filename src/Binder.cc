#include "Binder.h"
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <string>
#include <vector>
#include <algorithm>
#include <dirent.h>
#include <windows.h>

using namespace std;


FILE* OpenFile(const string& filename, const string& opt) {
    FILE* fptr = fopen(filename.c_str(), opt.c_str());
    if (fptr == NULL) {
        fprintf(stderr, "Can not open file %s\n", filename.c_str());
        exit(EXIT_FAILURE);
    }

    return fptr;
}


Binder::Binder(const std::string& boss_path,
               const string& src_folder)
  :_boss_path(boss_path), _src_folder(src_folder) {}


Binder::~Binder() {}


void CopyBinFile(FILE* src, FILE* target) {
    char byte;
    while (fread(&byte, sizeof(byte), 1, src) != 0) {
        fwrite(&byte, sizeof(byte), 1, target);
    }
}


void WriteBinFile(FILE* file, string msg) {
    for (size_t i = 0; i < msg.length(); ++i) {
        char byte = msg.at(i);
        fwrite(&byte, sizeof(byte), 1, file);
    }
}


void Binder::BindTo(const std::string& host_file_name) {
    FILE* host_file = OpenFile(host_file_name, "wb");

    FILE* boss_file = OpenFile(_boss_path, "rb");
    CopyBinFile(boss_file, host_file);
    fclose(boss_file);

    AppendFilesTo(host_file);

    fclose(host_file);
}


vector<string> FilenamesInFolder(const string& folder_name) {
    DIR* folder = opendir(folder_name.c_str());
    if (folder == NULL) {
        fprintf(stderr, "Open folder %s failed\n",
                folder_name.c_str());
        exit(EXIT_FAILURE);
    }

    typedef struct dirent* DirentPtr;
    DirentPtr entry = readdir(folder);
    vector<string> filenames;
    for (; entry != NULL; entry = readdir(folder)) {
        string filename(entry->d_name);
        if (filename != "." && filename != "..") {
            filenames.push_back(filename);
        }
    }

    closedir(folder);
    return filenames;
}


const vector<string>&
Binder::Filenames() {
    if (_filenames.empty()) {
        _filenames = FilenamesInFolder(_src_folder);
    }

    return _filenames;
}


void Binder::AppendFilesTo(FILE* host_file) {
    auto filenames = this->Filenames();
    for (string filename : filenames) {
        string filepath = CompletePath(_src_folder, filename);
        FILE* src = OpenFile(filepath, "rb");

        WriteBinFile(host_file, MetaData(filename));
        CopyBinFile(src, host_file);
        fclose(src);
    }
}


std::string Binder::MetaData(const std::string& filename) {
    string exe_yn = "N";
    if (IsExecutableFile(filename)) {
        exe_yn = "Y";
    }
    return bounder() + filename + bounder() + exe_yn;
}


string Binder::CompletePath(const std::string& dir,
                            const std::string& filename) {
    return dir + "/" + filename;
}


string Binder::bounder() {
    static string b;
    if(b.length() == 0) {
        b = string("||========") + string("========||");
    }
    return b;
}


bool Binder::IsExecutableFile(const std::string& filename) {
    const string tail(".exe");
    int pos = filename.length() - tail.length();
    if (pos < 0) {
        return false;
    } else {
        string s = filename;
        for (size_t i = 0; i < s.length(); ++i) {
            s[i] = tolower(s[i]);
        }
        return filename.compare(pos, tail.length(), tail) == 0;
    }
}

bool CheckAndSkipBounder(char byte, FILE* file) {
    static const string bounder = Binder::bounder();

    if (byte != bounder.at(0)) {
        return false;
    }

    string readin(1, byte);

    size_t i;
    for (i = 1 ; i < bounder.length() ; ++i) {
        if (fread(&byte, sizeof(byte), 1, file) == 0) {
            int back_index = 1 - i;
            fseek(file, back_index, SEEK_CUR);
            return false;
        }
        readin.push_back(byte);
    }

    if (readin.compare(bounder) == 0) {
        return true;
    } else {
        int back_index = 1 - bounder.length();
        fseek(file, back_index, SEEK_CUR);
        return false;
    }
}


void Binder::SkipBossFile(FILE* fptr) {
    char byte;
    while (fread(&byte, sizeof(byte), 1, fptr) != 0) {
        if (CheckAndSkipBounder(byte, fptr)) {
            break;
        }
    }
}


bool SameName(string l_name, string s_name) {
    if (l_name.length() == s_name.length()) {
        return l_name == s_name;
    } else if (l_name.length() < s_name.length()) {
        swap(l_name, s_name);
    }

    size_t pos = l_name.length() - s_name.length();
    size_t len = s_name.length();
    return l_name.compare(pos, len, s_name) == 0;
}

void Binder::Decompress(const std::string& filename) {
    FILE* file = OpenFile(filename, "rb");

    SkipBossFile(file);

    bool all_file_decompressed = false;
    while (!all_file_decompressed) {
        Metadata metadata = ReadMetadata(file);
        if (SameName(filename, metadata.filename)) {
            metadata.filename = "temp." + metadata.filename;
        }

        FILE* target_file = OpenFile(metadata.filename, "wb");
        all_file_decompressed = PullOutFile(file, target_file);
        fclose(target_file);

        RunFileIfExecutable(metadata);
    }

    fclose(file);
}


bool Binder::PullOutFile(FILE* src, FILE* target) {
    char byte;
    while (true) {
        // end of source file
        if (fread(&byte, sizeof(byte), 1, src) == 0) {
            return true;
        }

        // found next file section
        if (CheckAndSkipBounder(byte, src)) {
            return false;
        }

        fwrite(&byte, sizeof(byte), 1, target);
    }
}


void Binder::RunFileIfExecutable(const Metadata& metadata) {
    if (metadata.executable) {
        const char* filepath = metadata.filename.c_str();
        ShellExecute(NULL, "open", filepath, NULL, NULL, 1);
    }
}


Binder::Metadata Binder::ReadMetadata(FILE* file) {
    char byte;
    string filename;
    bool executable = false;

    while (fread(&byte, sizeof(byte), 1, file) != 0) {
        if (CheckAndSkipBounder(byte, file)) {
            fread(&byte, sizeof(byte), 1, file);
            executable = (byte == 'Y');
            break;
        }
        filename.push_back(byte);
    }

    return Metadata(filename, executable);
}

