#ifndef _BINDER_H_
#define _BINDER_H_ "binder.h"

#include <vector>
#include <string>

class Binder {
public:
    explicit Binder(const std::string&,
                    const std::string&);
    ~Binder();
    void BindTo(const std::string&);

    static std::string bounder();
    static void Decompress(const std::string&);

private:
    struct Metadata {
        Metadata(const std::string& fn, bool eb)
            :filename(fn), executable(eb){}
        std::string filename;
        const bool executable;
    };

    const std::string _boss_path;
    const std::string _src_folder;
    std::vector<std::string> _filenames;

    void AppendFilesTo(FILE*);
    const std::vector<std::string>& Filenames();

    static std::string
    CompletePath(const std::string&, const std::string&);
    static std::string MetaData(const std::string&);
    static bool IsExecutableFile(const std::string&);
    static Metadata ReadMetadata(FILE*);
    static void SkipBossFile(FILE*);
    static void RunFileIfExecutable(const Metadata&);
    static bool PullOutFile(FILE*, FILE*);
};  // end class Binder

#endif // _BINDER_H_
